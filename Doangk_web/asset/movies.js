const api = 'https://api.themoviedb.org/3/search/movie?api_key=70fb26dd3e849c4a4dc5f359dfb6566a';
const scr = 'https://image.tmdb.org/t/p/original';
const details = 'https://api.themoviedb.org/3/movie/';
const lastest = 'https://api.themoviedb.org/3/discover/movie?api_key=70fb26dd3e849c4a4dc5f359dfb6566a';

async function evtSubmit(entry) {
    entry.preventDefault();
    const searchInput = $('form input').val();
    choice = $('#timkiem').val();
    if (choice == 1) {
        const str = `${api}&query=${searchInput}`;
        loading();
        const response = await fetch(str);
        const rs = await response.json();
        getMovies(rs.results);
    }
    else if (choice == 2) {
        const req = `https://api.themoviedb.org/3/search/person?api_key=70fb26dd3e849c4a4dc5f359dfb6566a&query=${searchInput}`;
        loading();
        const resCast = await fetch(req);
        const rep = await resCast.json();
        filmCast(rep.results);
    }

}
function getMovies(ms) {
    $('#main').empty();
    for (const m of ms) {
        $('#main').append(`
            <div class="col-md-6 py-1">
            <div class="card mb-3 shadow h-100" onclick="loadDetails(${m.id})">
                <div class="row no-gutters">
                  <div class="col-md-4">
                    <img src="${scr}${m.poster_path}" class="card-img" alt="${m.title}">
                  </div>
                  <div class="col-md-8">
                    <div class="card-body">
                      <h5 class="card-title">${m.title}</h5>
                      <p class="card-text">${m.overview}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            `);
    }
}
async function loadDetails(id) {
    const reqStr = `${details}${id}?api_key=70fb26dd3e849c4a4dc5f359dfb6566a`;
    loading();
    const response = await fetch(reqStr);
    const movie = await response.json();
    getMoviesDetails(movie, id);
}
async function getMoviesDetails(m, id) {
    loading();
    const combine = `https://api.themoviedb.org/3/movie/${id}/images?api_key=70fb26dd3e849c4a4dc5f359dfb6566a`;
    const response = await fetch(combine);
    const rep = await response.json();

    const combineCast = `https://api.themoviedb.org/3/movie/${id}/credits?api_key=70fb26dd3e849c4a4dc5f359dfb6566a`;
    const castRespon = await fetch(combineCast);
    const repCast = await castRespon.json();

    const reviewUrl = `https://api.themoviedb.org/3/movie/${id}/reviews?api_key=70fb26dd3e849c4a4dc5f359dfb6566a`;
    const resReview = await fetch(reviewUrl);
    const preview = await resReview.json();
    $('#main').empty();
    $(`
            <!-- Page Content -->
            <div class="container">         
            <!-- Portfolio Item Heading -->
            <h1 class="my-4 text-primary">${m.original_title}
            </h1>       
            <!-- Portfolio Item Row -->
            <div class="row">           
                <div class="col-md-7 ">
                <img class="img-fluid" src="${scr}${m.poster_path}" >
                </div>      
                <div class="col-md-5 ">
                <h3 class="my-3 text-primary">Film Description</h3>
                <p class="text-primary">${m.overview}</p>
                <h3 class="my-3 text-primary">Genres</h3>
                <ul class="text-primary">
                    <li>${m.genres[0].name}</li>
                    <li>${m.genres[1].name}</li>
                    <li>${m.genres[2].name}</li>
                </ul>
                <h3 class="my-3 text-primary">Info</h3>
                <p class="text-primary">Release Date: ${m.release_date}</p>
                <h3 class="my-3 text-primary ">Cast</h3>              
                <ul id="cast" class=" text-primary cast-section" style="display: inline-block;">              
                </ul>
                </div>            
            </div>
            <!-- /.row -->          
            <!-- Related Projects Row -->
            <h3 class="my-4 text-primary">Movie scenes</h3>         
            <div class="row">    
                <div class="col-md-4 col-sm-6 mb-4">
                        <img class="img-fluid" src="${scr}${rep.backdrops[1].file_path}">
                </div>
                <div class="col-md-4 col-sm-6 mb-4">
                        <img class="img-fluid" src="${scr}${rep.backdrops[2].file_path}" >
                </div>
                <div class="col-md-4 col-sm-6 mb-4">
                        <img class="img-fluid" src="${scr}${rep.backdrops[3].file_path}" >
                </div>        
            </div>
            <!-- /.row -->          
            <!-- /.row -->
            <h3 class="my-4 text-primary">Reviews</h3>          
            <div id="review" class="row">        
            </div>
            <!-- /.row -->
            </div>
            <!-- /.container -->          
    `).appendTo('#main');
    var length = repCast.cast.length;
    for (i = 0; i < length; i++) {
        $('#cast').append(`
        <li  style="float: left; list-style: outside none none; width: 50%;">
            <a href="#"  onclick="actorInfo(${repCast.cast[i].id})">${repCast.cast[i].name}</a>
        </li>`);
    }
    for (var Item of preview.results) {
        $('#review').append(`
            <div class="card w-100">
            <div class="card-body">
                <h5 class="card-title">${Item.author}</h5>
                <p class="card-text">${Item.content}</p>
                <a href="${Item.url}" class="btn btn-primary">Review Detail</a>
            </div>
            </div>
        `);
    }
}
function filmCast(file) {
    $('#main').empty();
    for (const n of file) {
        $('#main').append(`
                <div class="col-sm-3">
                <div class="card h-100" >
                <img src="${scr}${n.profile_path}" class="card-img-top width="60%" " alt="${n.name}">
                <div class="card-body">
                <h5 class="card-title">${n.name}</h5>
                <p class="card-text"> Known for:  
                    <ul>   
                    </ul>      
                </p>           
                <a href="#" class="btn btn-primary" onclick="actorInfo(${n.id})">Go to Description</a>
                </div>
            </div>
            </div>
        `);
    }
}
async function actorInfo(id) {
  console.log("vào hàm");
    loading();
    const getActorId = `https://api.themoviedb.org/3/person/${id}?api_key=70fb26dd3e849c4a4dc5f359dfb6566a`;
    const response = await fetch(getActorId);
    const rep = await response.json();

    const getActorCredit = `https://api.themoviedb.org/3/person/${id}/movie_credits?api_key=70fb26dd3e849c4a4dc5f359dfb6566a`;
    const resCredit = await fetch(getActorCredit);
    const repCredit = await resCredit.json();
    console.log("flag1");
    $('#main').empty();
    $('#main').append(`
    
                <!-- Page Content -->
                <div class="container">    
                <!-- Portfolio Item Heading -->
                <h1 class="my-4 text-primary">${rep.name}
                </h1>     
                <!-- Portfolio Item Row -->
                <div class="row"> 
                    <div class="col-md-7 ">
                    <img class="img-fluid" src="${scr}${rep.profile_path}" >
                    </div>
                    <div class="col-md-5 ">
                    <h3 class="my-3 text-primary">Biography</h3>
                    <p class="text-primary">${rep.biography}</p>
                    <h3 class="my-3 text-primary">INFO</h3>
                    <ul class="text-primary">
                        <li>Birthday: ${rep.birthday}</li>
                        <li>Place of birth: ${rep.place_of_birth}</li>
                        <li>Popularity: ${rep.popularity}</li>
                    </ul>
                    </div>               
                </div>
                <!-- /.row -->               
                <!-- Related Projects Row -->
                <h3 class="my-4 text-primary">Film</h3>
                <div id="filmlist" class="row">
                                <div  class="col-md-6 py-1" >
                                    <div class="card mb-3 shadow h-100">
                                        <div class="row no-gutters">
                                            <div class="col-md-4">
                                                <img src="..." class="card-img" alt="...">
                                            </div>
                                        <div class="col-md-8">
                                             <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text"> abcd </p>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>               
                </div>
                <!-- /.row -->                
                </div>
                <!-- /.container -->          
    `);
    $('#filmlist').empty();
    for (const film of repCredit.cast) {
        $('#filmlist').append(`
                    <div class="col-md-6 py-1">
                    <div class="card mb-3 shadow h-100" onclick="loadDetails(${film.id})">
                        <div class="row no-gutters">
                        <div class="col-md-4" >
                            <img src="${scr}${film.poster_path}" class="card-img" alt="${film.title}">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                            <h5 class="card-title">${film.title}</h5>
                            <p class="card-text">${film.overview}</p>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
        `);
    }
    console.log("flag2");
}
async function getlastest() {
    $('#main').empty();
    loading();
    var n = 1;
    const pages = `${lastest}&page=${n}`;
    const response = await fetch(pages);
    const rep = await response.json();
    getMovies(rep.results);
}
function loading() {
    $('#main').empty();
    $('#main').append(`
        <div class="spinner-border text-primary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    `);
}